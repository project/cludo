# Cludo Plugin

## Introduction

The Cludo plugin bridges the gap between Drupal and the Cludo
Platform. You are now able to put your Cludo results to use
where they are most valuable – during your content creation and editing
process. With analytics and content insights always at hand, contributors
can test, fix, and optimize their work continuously. Once the detected issues
have been assessed, you can directly re-recheck the relevant page and see if
further actions are needed. Delivering a superior digital experience has never
been more efficient and convenient.

## Installation and configuration

Cludo Plugin can be installed like any other Drupal module.
Place it in the modules directory for your site and enable it
on the `admin/modules` page.

## Frequently Asked Questions

Who can use this plugin?
The plugin requires a Cludo subscription to be used.
Signup for a [FreeTrial](https://my.cludo.com/freetrial “Free trial”)
to test it out.

Where can I see the overlay?
The overlay is visible when editing a page.

I don't see the overlay, whats wrong?
Did you remember to turn off your adblocker? Some adblockers does not like
our iframe overlay.

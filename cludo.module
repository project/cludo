<?php

/**
 * @file
 * Drupal Module: Cludo’s Drupal Plugin.
 *
 * Adds the required Javascript to node forms pages for show a little box
 * with the results from the latest scan of the current page.
 */

use Drupal\cludo\CludoLibrary;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function cludo_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.cludo':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Cludo plugin bridges the gap between Drupal and the Cludo Platform. You are now able to put your Cludo results to use where they are most valuable – during your content creation and editing process. With analytics and content insights always at hand, contributors can test, fix, and optimize their work continuously. Once the detected issues have been assessed, you can directly re-recheck the relevant page and see if further actions are needed. Delivering a superior digital experience has never been more efficient and convenient.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_page_alter().
 */
function cludo_page_bottom(array &$page_bottom) {
  if (\Drupal::service('router.admin_context')->isAdminRoute()) {
    CludoLibrary::attachCmsOverlayScript($page_bottom);
  }
}

/**
 * Implements hook_form_node_form_alter().
 */
function cludo_form_node_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  $form_object = $form_state->getFormObject();

  if ($form_object instanceof EntityFormInterface) {
    $entity = $form_object->getEntity();
    if (!$entity->isNew()) {
      try {
        if (!\Drupal::service('router.admin_context')->isAdminRoute()) {
          CludoLibrary::attachCmsOverlayScript($form);
        }
        CludoLibrary::attachJsData($form, $entity->toUrl());
      }
      catch (EntityMalformedException $e) {
        \Drupal::logger('cludo')->error($e->getMessage());
      }
    }
  }
}
